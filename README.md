# webring.archaeo.social

[![status-badge](https://ci.codeberg.org/api/badges/12967/status.svg)](https://ci.codeberg.org/repos/12967)

Configuration files for the webring at <https://webring.archaeo.social>, running [go-webring](https://sr.ht/~amolith/go-webring).

## Deployment

Changes are automatically deployed to [fly.io](https://fly.io/) on push to the remote repository at <https://codeberg.org/archaeo-social/webring>, using the configuration in `fly.toml`.

## Updating the list of members

To update the list of members, edit the `members` file.
This contains one line per member with in the format explained at <https://sr.ht/~amolith/go-webring/#with-custom-files>.

## Updating go-webring

To update the version of go-webring used, edit the line `ARG v=...` in `Dockerfile`.
Since go-webring does not, as of writing, use tagged releases, you will need to specify the SHA hash of the commit to use.

Don't forget to also update the go version the docker image is based on, in the first line of the Dockerfile.
This should match the version specififed by go-webring in `go.mod`.
