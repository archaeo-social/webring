FROM golang:1.17

# go-webring does not have tagged releases, so specify the specific commit we
# want to run here
ARG v=c1a0465e4cc8353ef01e83050908863ffe328dac

# fetch go-webring from source repository
WORKDIR /app
RUN git init . && git remote add origin https://git.sr.ht/~amolith/go-webring
RUN git fetch --depth 1 origin $v && git checkout FETCH_HEAD

# build go-webring
RUN go mod download && go mod verify
RUN CGO_ENABLED=0 GOOS=linux go build -o /go-webring

# copy custom files
WORKDIR /
COPY members ./
COPY index.html ./
ADD static static/

# run go-webring
EXPOSE 2857
ENTRYPOINT ./go-webring -c $CONTACT -H $HOST -i $INDEX -l $LISTEN -m $MEMBERS -v $VALIDATIONLOG
